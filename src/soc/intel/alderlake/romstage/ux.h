/* SPDX-License-Identifier: GPL-2.0-only */

void ux_inform_user_of_update_operation(const char *name);
